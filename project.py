import math
import re
import numpy as np

train_files = ['heart-train.txt']#, 'simple-train.txt', 'vote-train.txt']
test_files = ['heart-test.txt']#, 'simple-test.txt', 'vote-test.txt']

def file_to_arrs(input_file):
	with open(input_file, 'rb') as train:
		lines = train.readlines()
		num_vars = int(lines[0])
		num_vectors = int(lines[1])
		xvars = np.zeros(shape=(num_vectors, num_vars))
		yvars = np.zeros(num_vectors)
		for row, line in enumerate(lines[2:]):
			stripped = ''.join(line.split(" ")).strip()
			for col in xrange(num_vars):
				xvars[row, col] = int(stripped[col])
			yvars[row] = int(stripped[num_vars+1])
	return xvars, yvars

def naive_bayes(xvars, yvars, laplace):
	p_y = find_p_y(yvars, laplace)
	num_cols = len(xvars[0])
	p_x = find_p_x(xvars, yvars, laplace)
	return p_x, p_y

def find_p_y(yvars, laplace):
	if laplace: 
		return float(sum(yvars)+1)/(len(yvars)+2)
	return float(sum(yvars))/len(yvars)

def find_p_x(xvars, yvars, laplace):
	if laplace:
		p_x = np.ones(shape=(2,len(xvars[0])))
		xCount = np.ones(len(xvars[0]))
		totalVectors = len(xvars) + 2
	else:
		p_x = np.zeros(shape=(2,len(xvars[0])))
		xCount = np.zeros(len(xvars[0]))
		totalVectors = len(xvars)
	cols = len(xvars[0])
	for col in xrange(cols):
		for row in xrange(len(yvars)):
			xvar = xvars[row][col]
			if yvars[row]:
				p_x[xvar][col] += 1 # number of times y is 1 with that x value
			if xvar:
				xCount[col] += 1 # of times x = 1 in that column
		p_x[0][col] = float(p_x[0][col])/(totalVectors-xCount[col]) # janky way of finding # zeros
		p_x[1][col] = float(p_x[1][col])/xCount[col]
	return p_x

def test_bayes(p_x, p_y, xvars, yvars):
	correct = 0
	rows = len(yvars)
	cols = len(xvars[0])
	for row in xrange(rows):
		yvar = yvars[row]
		p_0, p_1 = 0, 0
		for col in xrange(cols):
			xvar = xvars[row][col]
			if p_x[xvar][col] == 1: p_0 = float('-inf')
			elif p_x[xvar][col] == 0: p_1 = float('-inf')
			else:
				p_0 += math.log((1-p_x[xvar][col]))
				p_1 += math.log(p_x[xvar][col])
		p_1 += math.log(p_y)
		p_0 += math.log((1-p_y))
		if (p_1 >= p_0 and yvar) or (p_1 < p_0 and not yvar):
			correct += 1
	return float(correct)/rows



def log_reg(xvars, yvars):
	xvars = add_alpha(xvars)
	epochs = 10000
	n = .00001
	num_rows = len(xvars)
	num_cols = len(xvars[0])
	betas = np.zeros(num_cols)
	for e in xrange(epochs):
		print e
		gradient = np.zeros(num_cols)
		z = compute_z(betas, xvars) # compute z beforehand so we can update betas in loop
		for col in xrange(num_cols):
			for row in xrange(num_rows):
				denom = float(1 + math.exp(-1*z[row]))
				gradient[col] += xvars[row][col]*(yvars[row] - float(1)/denom)
		betas += n*gradient
	return betas

def add_alpha(xvars):
	alpha = np.ones(shape=(len(xvars), 1))
	xvars = np.append(xvars, alpha, 1)
	return xvars

def compute_z(betas, xvars):
	z = np.zeros(len(xvars))
	for row in xrange(len(xvars)):
		for col in xrange(len(xvars[0])):
			z[row] += betas[col]*xvars[row][col]
	return z

def test_log(testx, testy, betas):
	correct = 0
	for row in xrange(len(testx)):
		p = 0
		for col in xrange(len(testx[0])):
			p+=testx[row][col]*betas[col]
		if (testy[row] and p>=.5) or (not testy[row] and p<.5):
			correct += 1
	return float(correct)/len(testy)

def main():
	for train_file, test_file in zip(train_files, test_files):
		xvars, yvars = file_to_arrs(train_file)
		p_x, p_y = naive_bayes(xvars,yvars, 0)
		testx, testy = file_to_arrs(test_file)
		print "%s MLE: %s" % (test_file, test_bayes(p_x, p_y, testx, testy))
		p_x, p_y = naive_bayes(xvars,yvars, 1)
		testx, testy = file_to_arrs(test_file)
		print "%s Laplace Bayes: %s" % (test_file, test_bayes(p_x, p_y, testx, testy))
		betas = log_reg(xvars, yvars)
		print betas
		print "%s Logistic Regression: %s" % (test_file, test_log(testx, testy, betas))

if __name__ == "__main__":
    main()