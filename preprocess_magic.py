from magic_data import dataset
from project import *
import random

def gen_train_and_test_set(dataset):
	train_set = []
	test_set = []
	for i, data in enumerate(dataset):
		if (i>12332 and (i%400) == 1) or (i<=6688 and i%400 == 1):
			train_set.append(data)
		elif (i%2) == 0: # (i>12332 and (i%2) == 0) or (i<=6688 and i%2 == 0):
			test_set.append(data)
	return train_set, test_set

def convert_to_binary(dataset, averages):
	binary = []
	for row in dataset:
		bin = []
		for col, val in enumerate(row[:-1]):
			bin.append(int(val>averages[col]))
		bin.append(int(row[-1]=='g'))
		binary.append(bin)
	return binary

def get_averages(dataset):
	zipped = zip(*dataset)
	averages = []
	for i in xrange(len(zipped)-1):
		h = [x for j, x in enumerate(zipped[i]) if zipped[-1][j] == 'h']
		averages.append(sum(h)/len(h))
	return averages


def main():
	train_set, test_set = gen_train_and_test_set(dataset)
	averages = get_averages(train_set)
	train_set = convert_to_binary(train_set, averages)
	test_set = convert_to_binary(test_set, averages)
	xvars = [row[:-1] for row in train_set]
	yvars = [row[-1] for row in train_set]
	p_x, p_y = naive_bayes(xvars,yvars, 0)
	testx = [row[:-1] for row in test_set]
	testy = [row[-1] for row in test_set] 
	print "MLE: %s" % (test_bayes(p_x, p_y, testx, testy))
	p_x, p_y = naive_bayes(xvars,yvars, 1)
	print "Laplace Bayes: %s" % (test_bayes(p_x, p_y, testx, testy))
	betas = log_reg(xvars, yvars)
	print "Logistic Regression: %s" % (test_log(testx, testy, betas))



if __name__ == "__main__":
    main()